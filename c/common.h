#pragma once

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#define _USE_MATH_DEFINES
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#define assert_exist(thing, msg) if (!(thing)) { fprintf(stderr, "<%d: %s\n", __LINE__, msg); exit(-1); }

typedef enum {CAR_POSITIONS=1, TURBO_START, TURBO_END, CRASH, RESPAWN,
    LAP_FINISHED, FINISHED, DNF, YOUR_CAR, GAME_INIT, GAME_START, TURBO_AVAILABLE} MsgType;

typedef struct {
    int type;
    int tick;
    void* data;
    void* json_value;
} Msg;

typedef struct {
    double len;     // 0 for bend
    double r;       // 0 for straight
    double angle;   // negative: left, positive: right, 0: straight
    double r_angle; // angle in radius instead of degrees
    bool is_switch;
} Piece;

typedef struct {
    double d;
    double v;  // d diff
    double da; // drift angle diff
    double r;  // calculated radius by lane and switch
    double route_len; // total len of current route

    int start_lane;
    int end_lane;
    int lap;
    int piece;
    double in_piece_distance;
    double angle;
} Place;

typedef struct {
    char* id; // name \0 color \0
    int id_len;

    int places_len;
    Place places[5];

    bool turbo_used;
    bool finished;
    int crash_ticks;
} Car;

typedef struct {
    int pieces_len;
    int cars_len;
    int lanes_len;
    int my_car; // ordered for alignment
    Piece* pieces;
    Car* cars;
    double* lanes;

    int total_laps;
    double car_width;
    double car_length;
    double car_gfp; // guide flag position

    // car control history
    double last_throttle;
    double last_last_throttle;
    int turbo_ticks;
    double turbo_factor;
    bool can_turbo;
    bool in_turbo;
} Game;

extern Game game;

double piece_rest_len(Place* place);
