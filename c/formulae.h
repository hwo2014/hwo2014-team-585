#pragma once
#include "common.h"

// acceleration:
// a = C1*thr - C2*v
typedef struct {
    int64_t trained;
    double c1;
    double c2;
} FAccel;

// drift angular velocity:
// next_da = C1 da + C2 v * angle + C3 v + C4 v * v * (r ^ -0.5)
typedef struct {
    int64_t trained;
    double c1;
    double c2;
    double c3;
    double c4;
} FDrift;

extern FAccel faccel;
extern FDrift fdrift;

void train_accel(double thr1, double thr2, Place* p0, Place* p1, Place* p2);
void train_drift_tmp(Place* p1, double da1, Place* p2, double da2);
void train_drift(Place* p1, double da1, Place* p2, double da2, Place* p3, double da3, Place* p4, double da4);

double calc_a(Place* p, double throttle, FAccel* fa);
// for easier test, FDrift as param
double calc_da(Place* p, FDrift* fd);

// util
int bend_direction(Place* p);
double drift_threshold_speed(double r);
double throttle_for_v(double origin_v, double desired_v);
