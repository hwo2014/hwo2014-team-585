double coe = 1.0/(
  m.a1*m.b2*m.c3*m.d4-m.a1*m.b2*m.c4*m.d3-m.a1*m.b3*m.c2*m.d4+m.a1*m.b3*m.c4*m.d2+m.a1*m.b4*m.c2*m.d3-m.a1*m.b4*m.c3*m.d2-m.a2*m.b1*m.c3*m.d4+m.a2*m.b1*m.c4*m.d3+m.a2*m.b3*m.c1*m.d4-m.a2*m.b3*m.c4*m.d1-m.a2*m.b4*m.c1*m.d3+m.a2*m.b4*m.c3*m.d1
  + m.a3 * (m.b1*m.c2*m.d4-m.b1*m.c4*m.d2-m.b2*m.c1*m.d4+m.b2*m.c4*m.d1+m.b4*m.c1*m.d2-m.b4*m.c2*m.d1)
  + m.a4 * (-m.b1*m.c2*m.d3+m.b1*m.c3*m.d2+m.b2*m.c1*m.d3-m.b2*m.c3*m.d1-m.b3*m.c1*m.d2+m.b3*m.c2*m.d1)
);
Matrix44 inversed = {
(-m.b4*m.c3*m.d2+m.b3*m.c4*m.d2+m.b4*m.c2*m.d3-m.b2*m.c4*m.d3-m.b3*m.c2*m.d4+m.b2*m.c3*m.d4)*coe, (m.a4*m.c3*m.d2-m.a3*m.c4*m.d2-m.a4*m.c2*m.d3+m.a2*m.c4*m.d3+m.a3*m.c2*m.d4-m.a2*m.c3*m.d4)*coe, (-m.a4*m.b3*m.d2+m.a3*m.b4*m.d2+m.a4*m.b2*m.d3-m.a2*m.b4*m.d3-m.a3*m.b2*m.d4+m.a2*m.b3*m.d4)*coe, (m.a4*m.b3*m.c2-m.a3*m.b4*m.c2-m.a4*m.b2*m.c3+m.a2*m.b4*m.c3+m.a3*m.b2*m.c4-m.a2*m.b3*m.c4)*coe,
(m.b4*m.c3*m.d1-m.b3*m.c4*m.d1-m.b4*m.c1*m.d3+m.b1*m.c4*m.d3+m.b3*m.c1*m.d4-m.b1*m.c3*m.d4)*coe, (-m.a4*m.c3*m.d1+m.a3*m.c4*m.d1+m.a4*m.c1*m.d3-m.a1*m.c4*m.d3-m.a3*m.c1*m.d4+m.a1*m.c3*m.d4)*coe, (m.a4*m.b3*m.d1-m.a3*m.b4*m.d1-m.a4*m.b1*m.d3+m.a1*m.b4*m.d3+m.a3*m.b1*m.d4-m.a1*m.b3*m.d4)*coe, (-m.a4*m.b3*m.c1+m.a3*m.b4*m.c1+m.a4*m.b1*m.c3-m.a1*m.b4*m.c3-m.a3*m.b1*m.c4+m.a1*m.b3*m.c4)*coe,
(-m.b4*m.c2*m.d1+m.b2*m.c4*m.d1+m.b4*m.c1*m.d2-m.b1*m.c4*m.d2-m.b2*m.c1*m.d4+m.b1*m.c2*m.d4)*coe, (m.a4*m.c2*m.d1-m.a2*m.c4*m.d1-m.a4*m.c1*m.d2+m.a1*m.c4*m.d2+m.a2*m.c1*m.d4-m.a1*m.c2*m.d4)*coe, (-m.a4*m.b2*m.d1+m.a2*m.b4*m.d1+m.a4*m.b1*m.d2-m.a1*m.b4*m.d2-m.a2*m.b1*m.d4+m.a1*m.b2*m.d4)*coe, (m.a4*m.b2*m.c1-m.a2*m.b4*m.c1-m.a4*m.b1*m.c2+m.a1*m.b4*m.c2+m.a2*m.b1*m.c4-m.a1*m.b2*m.c4)*coe,
(m.b3*m.c2*m.d1-m.b2*m.c3*m.d1-m.b3*m.c1*m.d2+m.b1*m.c3*m.d2+m.b2*m.c1*m.d3-m.b1*m.c2*m.d3)*coe, (-m.a3*m.c2*m.d1+m.a2*m.c3*m.d1+m.a3*m.c1*m.d2-m.a1*m.c3*m.d2-m.a2*m.c1*m.d3+m.a1*m.c2*m.d3)*coe, (m.a3*m.b2*m.d1-m.a2*m.b3*m.d1-m.a3*m.b1*m.d2+m.a1*m.b3*m.d2+m.a2*m.b1*m.d3-m.a1*m.b2*m.d3)*coe, (-m.a3*m.b2*m.c1+m.a2*m.b3*m.c1+m.a3*m.b1*m.c2-m.a1*m.b3*m.c2-m.a2*m.b1*m.c3+m.a1*m.b2*m.c3)*coe
};
