#include "strategy.h"
#include "formulae.h"
#include "server-conn.h"

static Car* my_car() {
    return game.cars + game.my_car;
}

static double throttle_for_constant_v() {
    double thr = faccel.c2 * my_car()->places->v / faccel.c1;
    return thr > 1.0 ? 1.0 : thr;
}

static bool have_data_for_drift_training() {
    Place* p = my_car()->places;
    bool result = p[0].da != 0.0 && p[1].da != 0.0 && p[2].da != 0.0 && p[3].da != 0.0 && p[4].da != 0.0;
    return result;
}

static int next_piece_index(int piece) {
    piece++;
    return piece == game.pieces_len ? 0 : piece;
}

// traing faccel
Strategy opening_strategy1() {
    if (my_car()->places_len >= 4) {
        Place* p = my_car()->places;
        train_accel(1, 1, p + 2, p + 1, p);
        printf("<accel: (%f, %f)\n", faccel.c1, faccel.c2);
    }
    printf("<opening1\n");

    Strategy s = {.throttle = 1.0, .turn = 0, .turbo = false};
    return s;
}

// assume no "all-straight" games
Strategy opening_strategy2() {
    Place* p = my_car()->places;
    printf("<opening2\n");

    if (have_data_for_drift_training()) {
        train_drift(p+4, p[3].da, p+3, p[2].da, p+2, p[1].da, p+1, p[0].da);
        printf("<drift (%f, %f, %f, %f)\n", fdrift.c1, fdrift.c2, fdrift.c3, fdrift.c4);
        // todo update last trained tick
        return normal_strategy();
    } else {
        // todo if piece not long enough, use turbo
        if (p->v > 7.0) {
            // nearly constant v, but keep slightly larger so we can get drift data
            double thr = throttle_for_constant_v();
            Strategy s = {.throttle = thr * 0.7 + 0.3, .turn = 0, .turbo = false};
            return s;
        } else {
            // full accel
            Strategy s = {.throttle = 1.0, .turn = 0, .turbo = false};
            return s;
        }
    }
}

static int will_crash_ticks(Place* p) {
    Place next_p = *p;
    int i;
    for (i = 0; i < 30; i++) {
        double da = calc_da(&next_p, &fdrift);

        next_p.in_piece_distance += next_p.v;
        if (next_p.in_piece_distance > next_p.route_len) {
            next_p.in_piece_distance -= next_p.route_len;
            next_p.piece = next_piece_index(next_p.piece);
            // todo choose to switch to inner lane
            next_p.start_lane = next_p.end_lane;
            calc_real_r_and_route_len(&next_p);
        }
        next_p.da = da;
        next_p.angle += da;

        if (i < 10) {
            if (fabs(next_p.angle) >= 59.0) {
                return i;
            }
        } else {
            if (fabs(next_p.angle) >= 40.0) {
                return i;
            }
        }
    }
    return -1;
}

Strategy non_crash_strategy() {
    Place* p = my_car()->places;
    int crash_tick = will_crash_ticks(p);
    double thr;
    if (crash_tick >= 0) {
        printf("<crash threat: %d\n", crash_tick);
        if (crash_tick < 15) {
            thr = 0; // urgent! -- todo consider turbo
        } else {
            double v = drift_threshold_speed(p->r);
            thr = throttle_for_v(p->v, v) / 2;
        }
    } else {
        printf("<will not crash\n");
        // in the border is quite sensitive to throttle, so we speed down if angle is too big
        if (p->angle > 44) {
            thr = throttle_for_v(p->v, p->v) * 0.9;
        } else {
            thr = 1.0;
        }
    }
    Strategy s = {.throttle = thr, .turn = 0, .turbo = false};
    return s;
}

Strategy normal_strategy() {
    Place* p = my_car()->places;
    bool is_straight = (game.pieces[p->piece].angle == 0.0);
    // todo calc many pcs
    bool is_end_game = (p->lap + 1 == game.total_laps) && p->piece + 1 == game.pieces_len;
    if (is_straight && is_end_game) {
        return end_straight_strategy();
    } else {
        return non_crash_strategy();
    }
}

Strategy end_straight_strategy() {
    printf("<end game straight\n");
    Strategy s = {.throttle = 1.0, .turn = 0, .turbo = game.can_turbo};
    return s;
}
