#pragma once

#include "common.h"

void connect_to(const char* hostname, const char* port);

Msg read_msg();
void free_msg(Msg msg);

void write_join(const char* bot_name, const char* bot_key);
void write_throttle(double value, int tick);
void write_ping();
void write_turbo();

void read_car_positions(Msg msg);
void read_car_id(void* data, Car* car);
void read_game_init(Msg msg);
void read_turbo(void* data);

// utils
void calc_real_r_and_route_len(Place* places);
bool is_my_car(void* data);
