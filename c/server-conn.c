#include "server-conn.h"
#include "common.h"
#include "json.h"
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>

static int server_fd;
static FILE* server_file = NULL;

static json_value* json_get_field(json_value* json, const char* key) {
    if (!json) {
        printf("<warn: get json field %s from null json object\n", key);
        return NULL;
    }
    if (json->type != json_object) {
        printf("<warn: get json field %s from non objec\n", key);
        return NULL;
    }
    for (long i = 0; i < json->u.object.length; i++) {
        if (strcmp(json->u.object.values[i].name, key) == 0) {
            return json->u.object.values[i].value;
        }
    }
    return NULL;
}

static int json_get_field_integer(json_value* json, const char* key) {
    json_value* v = json_get_field(json, key);
    if (!v) {
        fprintf(stderr, "<no such field %s", key);
        exit(-1);
    }
    if (v->type != json_integer) {
        fprintf(stderr, "<%s is not an integer\n", key);
        exit(-1);
    }
    return v->u.integer;
}

static double json_get_field_dbl(json_value* json, const char* key) {
    json_value* v = json_get_field(json, key);
    if (!v) {
        fprintf(stderr, "<no such field %s", key);
        exit(-1);
    }
    if (v->type != json_double) {
        fprintf(stderr, "<%s is not a double\n", key);
        exit(-1);
    }
    return v->u.dbl;
}

static void error(const char *fmt, ...) {
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static bool streql(const char* expected, const char* s, int len) {
    return (strlen(expected) == len && memcmp(expected, s, len) == 0);
}

static int find_car_by_id(json_value* json_id) {
    Car car;
    read_car_id(json_id, &car);
    for (int i = 0; i < game.cars_len; i++) {
        if (game.cars[i].id_len == car.id_len && memcmp(game.cars[i].id, car.id, car.id_len) == 0) {
            free(car.id);
            return i;
        }
    }
    free(car.id);
    fprintf(stderr, "<failed to find car: %s\n", json_get_field(json_id, "name")->u.string.ptr);
    exit(-1);
}

void connect_to(const char *hostname, const char *port) {
    int status;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    server_fd = socket(PF_INET, SOCK_STREAM, 0);
    if (server_fd < 0) error("failed to create socket");

    status = connect(server_fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    server_file = fdopen(server_fd, "r");
}

Msg read_msg() {
    char* buf = NULL;
    size_t len = 0;
    json_value* json;

    getline(&buf, &len, server_file);
    if (len > 0 && buf) {
        json = json_parse(buf, len);
        printf("%.*s", (int)len, buf);

    } else {
        json = NULL;
    }
    if (json) {
        int ty = -1;
        json_value* type_info = json_get_field(json, "msgType");

        if (type_info == NULL) {
            fprintf(stderr, "<missing msgType\n");
            exit(-1);
        }

        char* msg_type = type_info->u.string.ptr;
        int msg_type_len = type_info->u.string.length;
        if (streql("carPositions", msg_type, msg_type_len)) {
            ty = CAR_POSITIONS;
        } else if (streql("turboStart", msg_type, msg_type_len)) {
            ty = TURBO_START;
            // data.name, data.color
        } else if (streql("turboEnd", msg_type, msg_type_len)) {
            ty = TURBO_END;
            // data.name, data.color
        } else if (streql("crash", msg_type, msg_type_len)) {
            // data.name, data.color
            // todo: mark it as v=0 and non-exist
            ty = CRASH;
        } else if (streql("respawn", msg_type, msg_type_len)) {
            // data.name, data.color
            // todo: mark it as v=0 and exist
            ty = RESPAWN;
        } else if (streql("lapFinished", msg_type, msg_type_len)) {
            // ignore for now
            ty = LAP_FINISHED;
        } else if (streql("finished", msg_type, msg_type_len)) {
            // data.name, data.color
            ty = FINISHED;
        } else if (streql("dnf", msg_type, msg_type_len)) {
            // disqualified due to disconnect etc.
            // data.car.name, data.car.color
            ty = DNF;
        } else if (streql("gameInit", msg_type, msg_type_len)) {
            ty = GAME_INIT;
        } else if (streql("yourCar", msg_type, msg_type_len)) {
            ty = YOUR_CAR;
        } else if (streql("gameStart", msg_type, msg_type_len)) {
            ty = GAME_START;
        } else if (streql("turboAvailable", msg_type, msg_type_len)) {
            ty = TURBO_AVAILABLE;
        }
        if (ty != CAR_POSITIONS) {
            printf("%.*s", (int)len, buf);
        }
        free(buf);

        json_value* data = json_get_field(json, "data");
        json_value* tick = json_get_field(json, "gameTick");
        Msg msg = {ty, (tick ? tick->u.integer : -1), data, json};
        return msg;
    } else {
        Msg msg = {0, -1, NULL, NULL};
        return msg;
    }
}

void free_msg(Msg msg) {
    if (msg.json_value) {
        json_value_free(msg.json_value);
        msg.json_value = NULL;
        msg.data = NULL;
    }
}

void write_join(const char *bot_name, const char *bot_key) {
    dprintf(server_fd, "{\"msgType\": \"join\", \"data\": {\"name\": \"%s\", \"key\": \"%s\"}}\n", bot_name, bot_key);
}

void write_throttle(double throttle, int tick) {
    printf("<throttle:%f at:%d\n", throttle, tick);
    dprintf(server_fd, "{\"msgType\": \"throttle\", \"data\": %f, \"gameTick\": %d}\n", throttle, tick);
}

void write_turbo() {
    game.can_turbo = false;
    game.in_turbo = true;
    dprintf(server_fd, "{\"msgType\": \"turbo\", \"SONIC RAINBOOOOM!\"}\n");
}

void write_ping() {
    dprintf(server_fd, "{\"msgType\": \"ping\", \"data\": \"ping\"}\n");
}

void calc_real_r_and_route_len(Place* places) {
    Piece* pc = game.pieces + places->piece;
    if (places->start_lane == places->end_lane) {
        if (pc->r == 0.0) {
            places->r = pc->r;
            places->route_len = pc->len;
        } else {
            places->r = pc->r - game.lanes[places->start_lane];
            places->route_len = fabs(places->r * pc->r_angle);
        }
    } else {
        if (pc->r == 0.0) {
            double w = fabs(game.lanes[0] - game.lanes[1]);
            places->r = (w * w + pc->len * pc->len) / (4 * w);
            places->route_len = places->r * 4 * atan(w / pc->len);
        } else {
            double rstart = fabs(pc->r - game.lanes[places->start_lane]);
            double rend = fabs(pc->r - game.lanes[places->end_lane]);
            double ri = rstart > rend ? rend : rstart;
            double ro = rstart > rend ? rstart : rend;
            double a = fabs(pc->angle);
            double x = sqrt(ro*ro + ri*ri - 2.0*ro*ri*cos(a));
            double b = atan(ro*sin(a) / (ri - ro*cos(a)));
            places->r = x / (2.0 * cos(b));
            places->route_len = places->r * (M_PI - 2.0 * b);
        }
    }
}

void read_car_positions(Msg msg) {
    json_value* data = msg.data;
    for (int i = 0; i < data->u.array.length; i++) {
        json_value* car = data->u.array.values[i];
        json_value* id = json_get_field(car, "id");
        int i = find_car_by_id(id);

        Place* places = game.cars[i].places;
        // move memory back 1 pos
        memmove(places + 1, places, 4 * sizeof(Place));
        if (game.cars[i].places_len < 5) {
            game.cars[i].places_len++;
        }

        places->angle = json_get_field_dbl(car, "angle");

        json_value* car_piece = json_get_field(car, "piecePosition");
        places->piece = json_get_field_integer(car_piece, "pieceIndex");
        places->in_piece_distance = json_get_field_dbl(car_piece, "inPieceDistance");
        places->lap = json_get_field_integer(car_piece, "lap");

        json_value* lane = json_get_field(car_piece, "lane");
        places->start_lane = json_get_field_integer(lane, "startLaneIndex");
        places->end_lane = json_get_field_integer(lane, "endLaneIndex");

        // calc real r and route len
        calc_real_r_and_route_len(places);

        // calc d, v, da
        if (game.cars[i].places_len > 1) {
            if (places->piece == places[1].piece) {
                places->v = places->in_piece_distance - places[1].in_piece_distance;
            } else {
                places->v = places->in_piece_distance + (places[1].route_len - places[1].in_piece_distance);
            }
            places->da = places->angle - places[1].angle;
        } else {
            places->v = places->in_piece_distance;
            places->da = 0.0;
        }
        if (places->v < -0.0000000001) {
            printf("<error: v=%f (r=%f, angle=%f)\n", places->v, places->r, places->angle);
            exit(-1);
        }
    }
}

void read_car_id(void* id, Car* car) {
    json_value* name = json_get_field(id, "name");
    json_value* color = json_get_field(id, "color");
    car->id_len = name->u.string.length + color->u.string.length + 2;
    car->id = malloc(car->id_len);
    sprintf(car->id, "%.*s%c%.*s%c", name->u.string.length, name->u.string.ptr, '\0',
                                    color->u.string.length, color->u.string.ptr, '\0');
}

void read_game_init(Msg msg) {
    json_value* data = msg.data;
    data = json_get_field(data, "race");
    assert_exist(data, "missing race field!");

    printf("<reading pieces\n");
    json_value* pieces = json_get_field(json_get_field(data, "track"), "pieces");
    assert_exist(pieces, "missing pieces field!");
    game.pieces_len = pieces->u.array.length;
    game.pieces = malloc(game.pieces_len * sizeof(Piece));
    for (int i = 0; i < pieces->u.array.length; i++) {
        json_value* piece = pieces->u.array.values[i];
        json_value* radius = json_get_field(piece, "radius");
        if (radius) {
            game.pieces[i].r = ((radius->type == json_integer) ? radius->u.integer : radius->u.dbl);
            game.pieces[i].angle = json_get_field_dbl(piece, "angle");
            game.pieces[i].len = 0.0;
        } else {
            game.pieces[i].r = 0;
            game.pieces[i].angle = 0;
            game.pieces[i].len = json_get_field_dbl(piece, "length");
        }
        json_value* is_switch = json_get_field(piece, "switch");
        if (is_switch) {
            game.pieces[i].is_switch = is_switch->u.boolean;
        } else {
            game.pieces[i].is_switch = false;
        }
        game.pieces[i].r_angle = game.pieces[i].angle * (M_PI / 180.0);
    }

    printf("<reading cars\n");
    json_value* cars = json_get_field(data, "cars");
    assert_exist(cars, "missing cars field!");
    game.cars_len = cars->u.array.length;
    game.cars = malloc(game.cars_len * sizeof(Car));
    for (int i = 0; i < cars->u.array.length; i++) {
        json_value* car = cars->u.array.values[i];
        json_value* id = json_get_field(car, "id");
        json_value* dimensions = json_get_field(car, "dimensions");
        read_car_id(id, game.cars + i);
        game.cars[i].places_len = 0;
        game.car_width = json_get_field_dbl(dimensions, "width");
        game.car_length = json_get_field_dbl(dimensions, "length");
        game.car_gfp = json_get_field_dbl(dimensions, "guideFlagPosition");
    }

    printf("<reading laps\n");
    json_value* laps = json_get_field(json_get_field(data, "raceSession"), "laps");
    assert_exist(laps, "missing raceSession.laps field!");
    game.total_laps = laps->u.integer;

    printf("<reading lane width\n");
    json_value* lanes = json_get_field(json_get_field(data, "track"), "lanes");
    assert_exist(lanes, "missing track.lanes field!");
    game.lanes_len = lanes->u.array.length;
    game.lanes = malloc(game.lanes_len * sizeof(double));
    for (int i = 0; i < game.lanes_len; i++) {
        int idx = json_get_field_integer(lanes->u.array.values[i], "index");
        json_value* d = json_get_field(lanes->u.array.values[i], "distanceFromCenter");
        if (d->type == json_double) {
            game.lanes[idx] = d->u.dbl;
        } else if (d->type == json_integer) {
            game.lanes[idx] = d->u.integer;
        } else {
            error("bad lane distance from center");
        }
    }
}

void read_turbo(void* data) {
    game.turbo_ticks = json_get_field_integer(data, "turboDurationTicks");
    game.turbo_factor = json_get_field_dbl(data, "turboFactor");
    game.can_turbo = true;
    game.in_turbo = false;
}

bool is_my_car(void* data) {
    return find_car_by_id(data) == game.my_car;
}
