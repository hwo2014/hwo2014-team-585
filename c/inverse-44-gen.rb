# generate the inverse of matrix 4x4
# https://www.wolframalpha.com/input/?i=inverse+%7B%7Ba1%2Ca2%2Ca3%2Ca4%7D%2C%7Bb1%2Cb2%2Cb3%2Cb4%7D%2C%7Bc1%2Cc2%2Cc3%2Cc4%7D%2C%7Bd1%2Cd2%2Cd3%2Cd4%7D%7D

def translate s
  s.gsub(/(?<=[1234]) (?=[abcd])/, '*').gsub /([abcd][1234])/, 'm.\1'
end

coe = <<-SS.strip
1.0/(
  a1 b2 c3 d4-a1 b2 c4 d3-a1 b3 c2 d4+a1 b3 c4 d2+a1 b4 c2 d3-a1 b4 c3 d2-a2 b1 c3 d4+a2 b1 c4 d3+a2 b3 c1 d4-a2 b3 c4 d1-a2 b4 c1 d3+a2 b4 c3 d1
  + a3 * (b1 c2 d4-b1 c4 d2-b2 c1 d4+b2 c4 d1+b4 c1 d2-b4 c2 d1)
  + a4 * (-b1 c2 d3+b1 c3 d2+b2 c1 d3-b2 c3 d1-b3 c1 d2+b3 c2 d1)
)
SS

m = <<-SS.strip
-b4 c3 d2+b3 c4 d2+b4 c2 d3-b2 c4 d3-b3 c2 d4+b2 c3 d4 | a4 c3 d2-a3 c4 d2-a4 c2 d3+a2 c4 d3+a3 c2 d4-a2 c3 d4 | -a4 b3 d2+a3 b4 d2+a4 b2 d3-a2 b4 d3-a3 b2 d4+a2 b3 d4 | a4 b3 c2-a3 b4 c2-a4 b2 c3+a2 b4 c3+a3 b2 c4-a2 b3 c4
b4 c3 d1-b3 c4 d1-b4 c1 d3+b1 c4 d3+b3 c1 d4-b1 c3 d4 | -a4 c3 d1+a3 c4 d1+a4 c1 d3-a1 c4 d3-a3 c1 d4+a1 c3 d4 | a4 b3 d1-a3 b4 d1-a4 b1 d3+a1 b4 d3+a3 b1 d4-a1 b3 d4 | -a4 b3 c1+a3 b4 c1+a4 b1 c3-a1 b4 c3-a3 b1 c4+a1 b3 c4
-b4 c2 d1+b2 c4 d1+b4 c1 d2-b1 c4 d2-b2 c1 d4+b1 c2 d4 | a4 c2 d1-a2 c4 d1-a4 c1 d2+a1 c4 d2+a2 c1 d4-a1 c2 d4 | -a4 b2 d1+a2 b4 d1+a4 b1 d2-a1 b4 d2-a2 b1 d4+a1 b2 d4 | a4 b2 c1-a2 b4 c1-a4 b1 c2+a1 b4 c2+a2 b1 c4-a1 b2 c4
b3 c2 d1-b2 c3 d1-b3 c1 d2+b1 c3 d2+b2 c1 d3-b1 c2 d3 | -a3 c2 d1+a2 c3 d1+a3 c1 d2-a1 c3 d2-a2 c1 d3+a1 c2 d3 | a3 b2 d1-a2 b3 d1-a3 b1 d2+a1 b3 d2+a2 b1 d3-a1 b2 d3 | -a3 b2 c1+a2 b3 c1+a3 b1 c2-a1 b3 c2-a2 b1 c3+a1 b2 c3
SS
m = m.lines.map{|l|
  l.split('|').map do |item|
    "(#{translate item.strip})*coe"
  end.join ', '
}.join ",\n"

File.open 'inverse-44.inc', 'w' do |f|
  f.puts "double coe = #{translate coe};"
  f.puts "Matrix44 inversed = {\n#{m}\n};"
end
