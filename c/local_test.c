#ifdef WONDERBOLTS_TEST

#include "common.h"
#include "formulae.h"

#define near_eql(expected, actual) (fabs(actual - expected)/expected < 0.00001)

static void test_bend_direction() {
    Piece pc;
    game.pieces = &pc;
    game.pieces_len = 1;
    Place p;
    p.piece = 0;

    // straight switch
    pc.r = 0;
    pc.len = 3;
    pc.angle = 0;
    pc.r_angle = 0;
    pc.is_switch = true;
    p.start_lane = 0;
    p.end_lane = 1;
    p.in_piece_distance = 1.0;
    p.route_len = 2.1;
    if (bend_direction(&p) != 1) {
        printf("%d: expected turn right on switch former half, but got: %d\n", __LINE__, bend_direction(&p));
        exit(-1);
    }
    p.start_lane = 2;
    p.in_piece_distance = 1.1;
    if (bend_direction(&p) != 1) {
        printf("%d: expected turn right on switch later half, but got: %d\n", __LINE__, bend_direction(&p));
        exit(-1);
    }

    // bend switch
    pc.r = 4;
    pc.len = 0;
    pc.angle = -30;
    pc.r_angle = -30 * M_PI / 180;
    pc.is_switch = true;
    if (bend_direction(&p) != -1) {
        printf("%d: expected turn left on switch, but got: %d\n", __LINE__, bend_direction(&p));
        exit(-1);
    }
}

static void test_accel() {
    // a = C1*thr - C2*v
    double c1 = 0.5;
    double c2 = 0.6;
    Place p1, p2, p3;
    p1.v = 3.0;
    double thr1 = 0.3;
    double thr2 = 0.4;
    double a1 = c1 * thr1 - c2 * p1.v;
    p2.v = a1 + p1.v;
    double a2 = c1 * thr2 - c2 * p2.v;
    p3.v = a2 + p2.v;
    train_accel(thr1, thr2, &p1, &p2, &p3);
    if (!near_eql(faccel.c1, c1)) {
        printf("%d: c1=%f, but trained c1=%f", __LINE__, c1, faccel.c1);
        exit(-1);
    }
    if (!near_eql(faccel.c2, c2)) {
        printf("%d: c2=%f, but trained c2=%f", __LINE__, c2, faccel.c2);
        exit(-1);
    }
}

static void test_train_drift() {
    // da_next = C1 da - C2 v * angle + bend_direction * (- C3 v + C4 v * v / sqrt(r))
    // (make the bend_direction = 1)
    Piece p = {.r = 100, .angle = 90};
    game.pieces = &p;
    game.pieces_len = 1;
    double lanes[] = {0.0};
    game.lanes = lanes;
    game.lanes_len = 1;

    double c1 = 0.91;
    double c2 = 0.00124;
    double c3 = 0.3;
    double c4 = 0.038;
    FDrift fd = {.c1 = c1, .c2 = c2, .c3 = c3, .c4 = c4};

    double threshold_v = c3 * sqrt(100.0) / c4;

    Place p1 = {
        .v = threshold_v + 0.1, .r = 100, .da = 0,
        .start_lane = 0, .end_lane = 0, .piece = 0
    };
    p1.angle = 0;

    Place p2 = {
        .v = threshold_v + 0.1, .r = 100, .da = calc_da(&p1, &fd),
        .start_lane = 0, .end_lane = 0, .piece = 0
    };
    p2.angle = p2.da + p1.angle;

    Place p3 = {
        .v = threshold_v + 0.2, .r = 100, .da = calc_da(&p2, &fd),
        .start_lane = 0, .end_lane = 0, .piece = 0
    };
    p3.angle = p3.da + p2.angle;

    Place p4 = {
        .v = threshold_v + 0.25, .r = 100, .da = calc_da(&p3, &fd),
        .start_lane = 0, .end_lane = 0, .piece = 0
    };
    p4.angle = p4.da + p3.angle;

    fdrift.trained = 0;
    train_drift(&p1, p2.da, &p2, p3.da, &p3, p4.da, &p4, calc_da(&p4, &fd));
    if (!near_eql(fd.c1, fdrift.c1) || !near_eql(fd.c2, fdrift.c2) || !near_eql(fd.c3, fdrift.c3) || !near_eql(fd.c4, fdrift.c4)) {
        printf("%d: c1=%f c2=%f c3=%f c4=%f\n", __LINE__, fdrift.c1, fdrift.c2, fdrift.c3, fdrift.c4);
        exit(-1);
    }
}

void local_test() {
    test_bend_direction();
    test_accel();
    test_train_drift();
    printf("test finished\n");
}

#endif
