#pragma once
#include "common.h"

typedef struct {
    double throttle;
    bool turbo;
    int turn;
} Strategy;

Strategy opening_strategy1();
Strategy opening_strategy2();
Strategy non_crash_strategy();
Strategy normal_strategy();
Strategy end_straight_strategy();
