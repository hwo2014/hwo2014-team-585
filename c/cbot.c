#include "server-conn.h"
#include "common.h"
#include "formulae.h"
#include "strategy.h"

Game game;
#ifndef WONDERBOLTS_TEST
static bool server_test = false;
#endif

static int find_car(Car* car) {
    printf("<finding car\n");
    for (int i = 0; i < game.cars_len; i++) {
        if (game.cars[i].id_len  == car->id_len && memcmp(game.cars[i].id, car->id, car->id_len) == 0) {
            return i;
        }
    }
    fprintf(stderr, "<can not find car!");
    exit(-1);
}

static void init_game(int argc, char* argv[]) {
    if (argc != 5) {
        fprintf(stderr, "Usage: bot host port botname botkey\n");
        exit(-1);
    }

    printf("%s\n", "<connecting");
    connect_to(argv[1], argv[2]);
    printf("%s\n", "<joining");
    write_join(argv[3], argv[4]);
    printf("%s\n", "<joined");

    // init run time states
    game.last_throttle = 1.0;
    game.turbo_ticks = 0;
    game.turbo_factor = 1;
    game.can_turbo = false;
    game.in_turbo = false;

    // init by communication
    Msg msg;
    Car my_car = {NULL, 0};
    while ((msg = read_msg()).type) {
        switch (msg.type) {
            case GAME_START: {
                game.my_car = find_car(&my_car);
                // NOTE only respond throttle to:
                // data.msgType=='gameStart' || (data.msgType === 'carPositions' && data.gameTick))
                write_throttle(1.0, msg.tick);
                free_msg(msg);
                goto terminate;
            }
            case YOUR_CAR: {
                read_car_id(msg.data, &my_car);
                break;
            }
            case GAME_INIT: {
                read_game_init(msg);
                break;
            }
            case CAR_POSITIONS: {
                read_car_positions(msg);
                if (msg.tick >= 0) {
                    write_throttle(1.0, msg.tick);
                }
                break;
            }
            case TURBO_AVAILABLE: {
                read_turbo(msg.data);
                break;
            }
        }
        free_msg(msg);
    }

terminate:
    if (my_car.id) {
        free(my_car.id);
        my_car.id = NULL;
    }
}

#ifdef WONDERBOLTS_TEST
void local_test();
#endif

int main(int argc, char *argv[]) {
#   ifdef WONDERBOLTS_TEST
    local_test();
#   else
    const char* server_test_s = getenv("SERVER_TEST");
    if (server_test_s && strcmp("x", server_test_s) == 0) {
        server_test = true;
    }
    init_game(argc, argv);

    Msg msg;
    while ((msg = read_msg()).type) {
        if (msg.type == CAR_POSITIONS && msg.tick >= 0) {
            read_car_positions(msg);
            Strategy s;
            if (faccel.trained < 1) {
                s = opening_strategy1();
            } else {
                Place* p = game.cars[game.my_car].places;
                printf("<{v:%f, da:%f, angle:%f, last_angle:%f, pc:%d, lap:%d, route:%f}\n", p->v, p->da, p->angle, p[1].angle, p->piece, p->lap, p->route_len);
                if (fdrift.trained < 1) {
                    s = opening_strategy2();
                } else {
                    s = normal_strategy();
                }
            }
            printf("<strategy(%f %d %d %d %d)\n", s.throttle, s.turn, s.turbo, msg.tick, game.in_turbo);
            if (game.in_turbo) {
                if (s.throttle < 1.0) {
                    write_throttle(s.throttle / game.turbo_factor, msg.tick);
                } else {
                    write_throttle(s.throttle, msg.tick);
                }
            }
            if (s.turbo) {
                write_turbo();
            }
            game.last_last_throttle = game.last_throttle;
            game.last_throttle = s.throttle;
        } else {
            printf("<writing ping\n");
            write_ping();
            if (msg.type == TURBO_AVAILABLE) {
                read_turbo(msg.data);
            } else if (msg.type == TURBO_END) {
                if (is_my_car(msg.data)) {
                    game.in_turbo = false;
                }
            }
        }
        free_msg(msg);
    }
    printf("<game ended\n");
#   endif
    return 0;
}
