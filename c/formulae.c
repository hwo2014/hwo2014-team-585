#include "formulae.h"

FAccel faccel = {0};
FDrift fdrift = {-1, 0.9, 0.00125}; // finnland default c1, c2

typedef struct {
    double a1; double a2;
    double b1; double b2;
} Matrix22;

typedef struct {
    double a1; double a2; double a3; double a4;
    double b1; double b2; double b3; double b4;
    double c1; double c2; double c3; double c4;
    double d1; double d2; double d3; double d4;
} Matrix44;

static Matrix22 inverse22(Matrix22 m) {
    // todo: if it's very near 0...
    double quo = 1 / (m.a1 * m.b2 - m.a2 * m.b1);
    Matrix22 r = {m.b2 * quo, -m.a2 * quo, -m.b1 * quo, m.a1 * quo};
    return r;
}

static Matrix44 inverse44(Matrix44 m) {
    // todo: if it's very near 0...
    #include "inverse-44.inc"
    return inversed;
}

// a = C1*thr - C2*v
// need 3 ticks
void train_accel(double thr1, double thr2, Place* p1, Place* p2, Place* p3) {
    double a1 = p2->v - p1->v;
    double a2 = p3->v - p2->v;
    Matrix22 m = {thr1, -p1->v, thr2, -p2->v};
    m = inverse22(m);
    double c1 = m.a1 * a1 + m.a2 * a2;
    double c2 = m.b1 * a1 + m.b2 * a2;

    if (c1 > 0 && c2 > 0) {
    } else {
        // todo: error
        return;
    }

    // calibrate result
    faccel.c1 = (faccel.c1 * faccel.trained + c1) / (faccel.trained + 1);
    faccel.c2 = (faccel.c2 * faccel.trained + c2) / (faccel.trained + 1);
    faccel.trained++;
}

// get bend direction by:
// - piece info
// - place info: start/end lane, in_piece_distance, route_len
int bend_direction(Place* p) {
    bool is_straight = (game.pieces[p->piece].r == 0.0);
    if (is_straight) {
        if (p->start_lane == p->end_lane) {
            return 0;
        } else {
            if (p->in_piece_distance < p->route_len / 2.0) {
                // same sign as lane diff
                return p->end_lane > p->start_lane ? 1 : -1;
            } else {
                // negative as lane diff
                return p->end_lane > p->start_lane ? -1 : 1;
            }
        }
    } else {
        return game.pieces[p->piece].angle > 0.0 ? 1 : -1;
    }
}

// da_next = - C3 v + C4 v * v / sqrt(r)
void train_drift_tmp(Place* p1, double da1, Place* p2, double da2) {
    int bp[] = {0, bend_direction(p1), bend_direction(p2)};
    Matrix22 m = {
        -p1->v * bp[1], p1->v * p1->v / sqrt(p1->r) * bp[1],
        -p2->v * bp[2], p2->v * p2->v / sqrt(p2->r) * bp[2]
    };
    m = inverse22(m);

    // train rough c3, c4 with 2 ticks of data, because:
    // the first drift starts from 0 deg, 0 deg/tick, the first 2 terms are 0.
    // the second drift still has a relatively small deg and deg/tick, can assume the first 2 terms are near 0.
    // to be more precise, the first 2 terms are assumed to be finnland consts.
    da2 -= (fdrift.c1 * p2->da - fdrift.c2 * p2->v * p2->angle);

    double c3 = m.a1 * da1 + m.a2 * da2;
    double c4 = m.b1 * da1 + m.b2 * da2;

    if (c3 > 0 && c4 > 0) {
        // good
    } else {
        // todo: error
    }

    fdrift.c3 = c3;
    fdrift.c4 = c4;
    fdrift.trained = 0;
}

// da_next = C1 da - C2 v * angle + bend_direction * (- C3 v + C4 v * v / sqrt(r))
// need 5 ticks of non-0 angles
void train_drift(Place* p1, double da1, Place* p2, double da2, Place* p3, double da3, Place* p4, double da4) {
    int bp[] = {0, bend_direction(p1), bend_direction(p2), bend_direction(p3), bend_direction(p4)};
    Matrix44 m = {
        p1->da, -p1->v * p1->angle, -p1->v * bp[1], p1->v * p1->v / sqrt(p1->r) * bp[1],
        p2->da, -p2->v * p2->angle, -p2->v * bp[2], p2->v * p2->v / sqrt(p2->r) * bp[2],
        p3->da, -p3->v * p3->angle, -p3->v * bp[3], p3->v * p3->v / sqrt(p3->r) * bp[3],
        p4->da, -p4->v * p4->angle, -p4->v * bp[4], p4->v * p4->v / sqrt(p4->r) * bp[4]
    };
    m = inverse44(m);
    double c1 = m.a1*da1 + m.a2*da2 + m.a3*da3 + m.a4*da4;
    double c2 = m.b1*da1 + m.b2*da2 + m.b3*da3 + m.b4*da4;
    double c3 = m.c1*da1 + m.c2*da2 + m.c3*da3 + m.c4*da4;
    double c4 = m.d1*da1 + m.d2*da2 + m.d3*da3 + m.d4*da4;

    if (c1 > 0 && c2 > 0 && c3 > 0 && c4 > 0) {
        // good
    } else {
        // todo: error
        printf("> train error: %f, %f, %f, %f\n", c1, c2, c3, c4);
        return;
    }

    // calibrate result
    if (fdrift.trained < 0) {
        fdrift.trained = 0;
    }
    fdrift.c1 = (fdrift.c1 * fdrift.trained + c1) / (fdrift.trained + 1);
    fdrift.c2 = (fdrift.c2 * fdrift.trained + c2) / (fdrift.trained + 1);
    fdrift.c3 = (fdrift.c3 * fdrift.trained + c3) / (fdrift.trained + 1);
    fdrift.c4 = (fdrift.c4 * fdrift.trained + c4) / (fdrift.trained + 1);
    fdrift.trained++;
}

double calc_a(Place* p, double throttle, FAccel* fa) {
    return fa->c1 * throttle - fa->c2 * p->v;
}

double calc_da(Place* p, FDrift* fd) {
    double threshold = 0.0;
    if (p->r != 0.0) {
        threshold = p->v * (- fd->c3 + fd->c4 * p->v / sqrt(fabs(p->r)));
    }
    if (isnan(threshold) || threshold < 0.0) {
        threshold = 0.0;
    }
    return fd->c1 * p->da - fd->c2 * p->v * p->angle + bend_direction(p) * threshold;
}

double drift_threshold_speed(double r) {
    return fdrift.c3 / fdrift.c4 * sqrt(fabs(r));
}

double throttle_for_v(double origin_v, double desired_v) {
    double thr = ((desired_v - origin_v) + faccel.c2 * origin_v) / faccel.c1;
    if (thr > 1.0) {
        return 1.0;
    } else if (thr < 0.0) {
        return 0.0;
    } else {
        return thr;
    }
}
